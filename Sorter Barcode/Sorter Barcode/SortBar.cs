﻿using System.Linq;
using SorterBarcode.Models;

namespace SorterBarcode
{
    public class SortBar
    {
        public Configuration Configuration = new Configuration();

        public Pack GetPack(int pageCount) => new Pack(pageCount, Configuration.CurrentGroup);

        public Pack[] GetPacks(int[] pageCounts)
        {
            Configuration.CurrentGroup = Groups.Next(Configuration.CurrentGroup);
            return new Pack[] { };
        }

        public Pack[] GetPacks(int totalPacks, int pageCount) => GetPacks(Enumerable.Repeat(totalPacks, pageCount).ToArray());
    }
}