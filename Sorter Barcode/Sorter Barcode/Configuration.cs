﻿using SorterBarcode.Models;

namespace SorterBarcode
{
    public class Configuration
    {
        private GroupSequence _CurrentGroup { get; set; } = Groups.AvailableGroups[(int)Sequence.One];
        public GroupSequence CurrentGroup
        {
            get { return _CurrentGroup; }
            set { _CurrentGroup = value; }
        }

        

    }
}
