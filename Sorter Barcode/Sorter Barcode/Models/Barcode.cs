﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SorterBarcode.Models
{
    public class Barcode
    {

        public Barcode() { }

        public Barcode(GroupSequence group, PageSequence page, bool lastPage = false)
        {
            _CurrentGroup = group;
            _CurrentPage = page;
            LastPage = lastPage;
        }

        private GroupSequence _CurrentGroup { get; set; }
        public GroupSequence CurrentGroup
        {
            get { return _CurrentGroup; }
            set { _CurrentGroup = value; }
        }

        private PageSequence _CurrentPage { get; set; }
        public PageSequence CurrentPage
        {
            get { return _CurrentPage; }
            set { _CurrentPage = value; }
        }

        private bool _LastPage { get; set; }
        public bool LastPage
        {
            get { return _LastPage; }
            set { _LastPage = value; }
        }

        public bool IsValid()
        {
            if (CurrentGroup == null || CurrentPage == null || 
                CurrentGroup?.IsValid() == false || CurrentPage?.IsValid() == false)
                return false;
            return true;
        }

        public override string ToString()
        {
            return string.Format(
                "*{0}{1}0{2}*",
                CurrentGroup.ToString(),
                CurrentPage.ToString(),
                LastPage == true ? "2" : "0"
            );
        }
    }
}
