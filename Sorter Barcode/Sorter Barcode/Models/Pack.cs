﻿using System.Collections.Generic;

namespace SorterBarcode.Models
{
    public class Pack
    {
        public Pack(int totalPages, GroupSequence currentGroup)
        {
            for(int i = 0; i < (int)totalPages; i++)
                Barcodes.Add(new Barcode() {
                    CurrentGroup = currentGroup,
                    CurrentPage = Pages.AvailablePages[i],
                    LastPage = (i == totalPages -1 ? true : false) });
        }

        public Pack(int totalPages, GroupSequence currentGroup, int[] traySelect)
        {

        }
        
        public List<Barcode> Barcodes { get; set; } = new List<Barcode>();

        public string[] ToArray()
        {
            var BarcodeStrings = new List<string>();
            foreach (Barcode bc in Barcodes)
                BarcodeStrings.Add(bc.ToString());
            return BarcodeStrings.ToArray();
        }
    }
}
