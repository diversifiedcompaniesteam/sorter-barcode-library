﻿namespace SorterBarcode.Models
{
    public class GroupSequence
    {   
        private int _Index { get; set; }
        public int Index
        {
            get { return _Index; }
            set { _Index = value; }
        }

        private char _Value { get; set; }
        public char Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public bool IsValid()
        {
            return true;
        }
    }

    public static class Groups
    {
        public static readonly GroupSequence[] AvailableGroups =
        {
            new GroupSequence() { Index = 0, Value = '\u0047' }, //G
            new GroupSequence() { Index = 1, Value = '\u0038' }, //8
            new GroupSequence() { Index = 2, Value = '\u004F' }, //O
            new GroupSequence() { Index = 3, Value = '\u0034' }, //4
            new GroupSequence() { Index = 4, Value = '\u004B' }, //K
            new GroupSequence() { Index = 5, Value = '\u0043' }, //C
            new GroupSequence() { Index = 6, Value = '\u0053' }, //S
            new GroupSequence() { Index = 7, Value = '\u0032' }, //2
            new GroupSequence() { Index = 8, Value = '\u0049' }, //I
            new GroupSequence() { Index = 9, Value = '\u0041' }, //A
            new GroupSequence() { Index = 10, Value = '\u0051' }, //Q
            new GroupSequence() { Index = 11, Value = '\u0036' }, //6
            new GroupSequence() { Index = 12, Value = '\u004D' }, //M
            new GroupSequence() { Index = 13, Value = '\u0045' }, //E
            new GroupSequence() { Index = 14, Value = '\u0055' }, //U
        };

        public static GroupSequence Next(GroupSequence group) => Next(group.Index);
        public static GroupSequence Next(int group)
        {
            if(group <= 13 && group >= 0)
                return AvailableGroups[group++];

            return AvailableGroups[0];
        }

    }
}
