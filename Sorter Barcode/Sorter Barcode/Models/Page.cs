﻿namespace SorterBarcode.Models
{
    public class PageSequence
    {
        private int _Index { get; set; }
        public int Index
        {
            get { return _Index; }
            set { _Index = value; }
        }

        private char _Value { get; set; }
        public char Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public bool IsValid()
        {
            if (Index > 4 && Index < 0)
                return false;
            return true;
        }
    }

    public class Pages
    {
        public static readonly PageSequence[] AvailablePages =
        {
            new PageSequence() { Index = 0, Value = '\u0047' }, //G
            new PageSequence() { Index = 1, Value = '\u0038' }, //8
            new PageSequence() { Index = 2, Value = '\u004F' }, //O
            new PageSequence() { Index = 3, Value = '\u0034' }, //4
            new PageSequence() { Index = 4, Value = '\u004B' }, //K
        };

        public static PageSequence Next(PageSequence page) => Next(page.Index);
        public static PageSequence Next(int page)
        {
            if (page <= 3 && page >= 0)
                return AvailablePages[page++];

            return AvailablePages[0];
        }
    }
}
