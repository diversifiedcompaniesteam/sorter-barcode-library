﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SorterBarcode.Models;

namespace SorterBarcode.UnitTests
{
    [TestClass]
    public class BarcodeTest
    {
        [TestMethod]
        public void GetBarcodeLast()
        {
            Barcode bc = new Barcode()
            {
                CurrentGroup = Groups.AvailableGroups[(int)Sequence.One],
                CurrentPage = Pages.AvailablePages[(int)Sequence.One],
                LastPage = true
            };

            Assert.AreEqual(bc.ToString(), "*GG02*");
        }

        [TestMethod]
        public void GetBarcode()
        {
            Barcode bc = new Barcode()
            {
                CurrentGroup = Groups.AvailableGroups[(int)Sequence.One],
                CurrentPage = Pages.AvailablePages[(int)Sequence.One]
            };

            Assert.AreEqual(bc.ToString(), "*GG00*");
        }
    }
}
