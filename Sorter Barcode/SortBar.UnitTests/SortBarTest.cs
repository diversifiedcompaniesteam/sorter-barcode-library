﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SorterBarcode.Models;

namespace SorterBarcode.UnitTests
{
    [TestClass]
    public class SortBarTest
    {
        string[][] ValidPacks =
        {
            new string[] { "*GG02*" },
            new string[] { "*GG00*", "*G802*" },
            new string[] { "*GG00*", "*G800*", "*GO02*" },
            new string[] { "*GG00*", "*G800*", "*GO00*", "*G402*" },
            new string[] { "*GG00*", "*G800*", "*GO00*", "*G400*", "*GK02*" }
        };

        [TestMethod]
        public void GetPackTest()
        {
            for(int i = 0; i < 5; i++)
            {
                SortBar sortBar = new SortBar();
                CollectionAssert.AreEqual(sortBar.GetPack(i + 1).ToArray(), ValidPacks[i]);
            }
        }
    }
}
